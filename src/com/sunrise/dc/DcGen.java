package com.sunrise.dc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.cloudgarden.resource.SWTResourceManager;
import com.sunrise.gen.GeneratorMain;
import com.sunrise.gen.GeneratorProperties;
import com.sunrise.gen.provider.db.DbTableFactory;
import com.sunrise.gen.provider.db.model.Table;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class DcGen extends org.eclipse.swt.widgets.Composite {

	private Menu menu1;
	private MenuItem aboutMenuItem;
	private MenuItem contentsMenuItem;
	private Menu helpMenu;
	private MenuItem helpMenuItem;
	private Text db_drv;
	private Text db_url;
	private Label label6;
	private Label label5;
	private Label label4;
	private Label label3;
	private Label label2;
	private Label label1;
	private Button close_win;
	private Button done;
	private Button show_tables;
	private Label label7;
	private Label label8;
	private List tables_name;
	private Text out_path;
	private Text out_pack;
	private Text author;
	private Text db_pass;
	private Text db_user;

	{
		//Register as a resource user - SWTResourceManager will
		//handle the obtaining and disposing of resources
		SWTResourceManager.registerResourceUser(this);
	}

	public DcGen(Composite parent, int style) {
		super(parent, style);
		initGUI();
	}
	
	/**
	* Initializes the GUI.
	*/
	private void initGUI() {
		try {
			this.setSize(711, 475);
			this.setLayout(null);
			this.setToolTipText("数据中心代码平台代码生成器V1.0");
			{
				label1 = new Label(this, SWT.NONE);
				label1.setText("\u6570\u636e\u5e93URL\uff1a");
				label1.setBounds(12, 11, 109, 19);
				label1.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label1.setAlignment(SWT.RIGHT);
				label1.setSize(109, 19);
			}
			{
				label2 = new Label(this, SWT.NONE);
				label2.setText("\u6570\u636e\u5e93\u9a71\u52a8\uff1a");
				label2.setBounds(12, 37, 109, 20);
				label2.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label2.setAlignment(SWT.RIGHT);
			}
			{
				label3 = new Label(this, SWT.NONE);
				label3.setText("\u6570\u636e\u5e93\u7528\u6237\u540d\uff1a");
				label3.setBounds(12, 65, 109, 23);
				label3.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label3.setAlignment(SWT.RIGHT);
			}
			{
				label4 = new Label(this, SWT.NONE);
				label4.setText("\u6570\u636e\u5e93\u5bc6\u7801\uff1a");
				label4.setBounds(12, 90, 109, 20);
				label4.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label4.setAlignment(SWT.RIGHT);
			}
			{
				label5 = new Label(this, SWT.NONE);
				label5.setText("\u8f93\u51fa\u5305\u540d\uff1a");
				label5.setBounds(12, 116, 109, 20);
				label5.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label5.setAlignment(SWT.RIGHT);
			}
			{
				label6 = new Label(this, SWT.NONE);
				label6.setText("\u8f93\u51fa\u8def\u5f84\uff1a");
				label6.setBounds(12, 142, 109, 20);
				label6.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label6.setAlignment(SWT.RIGHT);
			}
			{
				label8 = new Label(this, SWT.NONE);
				label8.setText("作者名称：");
				label8.setBounds(12, 168, 109, 20);
				label8.setFont(SWTResourceManager.getFont("宋体", 11, 0, false, false));
				label8.setAlignment(SWT.RIGHT);
			}
			{
				db_url = new Text(this, SWT.NONE);
//				db_url.setText("jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 132.121.26.15)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = 132.121.26.17)(PORT = 1521))(LOAD_BALANCE = yes)(CONNECT_DATA =	(SERVER = DEDICATED)(SERVICE_NAME = testcent_s2)(FAILOVER_MODE =(TYPE = SELECT)(METHOD = BASIC)(RETRIES = 180)(DELAY = 5))))");
//				db_url.setText("jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.17.88)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = NEWMALL)))");
//				db_url.setText("jdbc:mysql://39.108.103.66:3306/xiumi?characterEncoding=utf8");
				db_url.setText("jdbc:mysql://127.0.0.1:3306/xiumi?characterEncoding=utf8");
				db_url.setBounds(129, 11, 573, 19);
				db_url.setSize(573, 19);
			}
			{
				db_drv = new Text(this, SWT.NONE);
//				db_drv.setText("oracle.jdbc.driver.OracleDriver");
				db_drv.setText("com.mysql.jdbc.Driver");
				db_drv.setBounds(129, 37, 573, 20);
				db_drv.setOrientation(SWT.HORIZONTAL);
			}
			{
				db_user = new Text(this, SWT.NONE);
				db_user.setText("root");
				db_user.setBounds(129, 64, 573, 20);
				db_user.setOrientation(SWT.HORIZONTAL);
			}
			{
				db_pass = new Text(this, SWT.NONE);
				db_pass.setText("K33Y77ZggZx");
//				db_pass.setText("123456");
				db_pass.setBounds(129, 90, 573, 20);
				db_pass.setOrientation(SWT.HORIZONTAL);
			}
			{
				out_pack = new Text(this, SWT.NONE);
				out_pack.setText("com.benfan.app.xiumi");
				out_pack.setBounds(129, 116, 573, 20);
			}
			{
				out_path = new Text(this, SWT.NONE);
				out_path.setText("d:/wcs_gen_code");
				out_path.setBounds(129, 142, 573, 20);
			}
			{
				author = new Text(this, SWT.NONE);
				author.setText("luoshan");
				author.setBounds(129, 168, 573, 20);
			}
			{
				tables_name = new List(this, SWT.V_SCROLL | SWT.BORDER | SWT.MULTI | SWT.BORDER);
				tables_name.setBounds(129, 194, 573, 232);
			}
			{
				label7 = new Label(this, SWT.NONE);
				label7.setAlignment(SWT.RIGHT);
				label7.setText("\u6570\u636e\u5e93\u8868\u540d\uff1a");
				label7.setBounds(12, 241, 109, 20);
				label7.setFont(SWTResourceManager.getFont("宋体",11,0,false,false));
			}
			{
				show_tables = new Button(this, SWT.PUSH | SWT.DOWN);
				show_tables.setText("\u663e\u793a\u8868\u540d");
				show_tables.setBounds(41, 263, 60, 30);
				show_tables.addSelectionListener(new SelectionAdapter() {   
					public void widgetSelected(SelectionEvent e) {   
						MainVO vo=new MainVO();
						vo.setDrv(db_drv.getText());
						vo.setUrl(db_url.getText());
						vo.setPack(out_pack.getText());
						vo.setPath(out_path.getText());
						vo.setUser(db_user.getText());
				        vo.setPass(db_pass.getText());
				        vo.setAuthor(author.getText());
				        GeneratorProperties.init(vo);
						tables_name.removeAll();
						try {							
							java.util.List ls=DbTableFactory.getInstance().getAllTables2();
							for(int i=0;i<ls.size();i++){
								Table t=(Table)ls.get(i);
								tables_name.add(t.getSqlName());
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}   
				});  
			}
			{
				done = new Button(this, SWT.PUSH | SWT.CENTER);
				done.setText("\u751f\u6210\u4ee3\u7801");
				done.setBounds(275, 438, 60, 30);
				done.addSelectionListener(new SelectionAdapter() {   
					public void widgetSelected(SelectionEvent e) {  
						String[] items=tables_name.getSelection();
						if(items!=null&&items.length>0){
							System.out.println("开始生成代码.....");
							items=tables_name.getSelection();
							for (int i = 0; i < items.length; i++) {  
								System.out.println(">>选中的表有>>>>>>>>>>>"+items[i]);  
							}
							MainVO vo=new MainVO();
							vo.setDrv(db_drv.getText());
							vo.setUrl(db_url.getText());
							vo.setPack(out_pack.getText());
							vo.setPath(out_path.getText());
							vo.setUser(db_user.getText());
					        vo.setPass(db_pass.getText());
					        vo.setSeletedData(items);
					        try {
								GeneratorMain.genCode(vo);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}else{
							int style = SWT.ICON_QUESTION |SWT.YES;
							MessageBox messageBox = new MessageBox(getShell(), style);
							messageBox.setMessage("您没有选择任何一张表生成代码。");
						    int rc = messageBox.open();
						}
					}   
				});  
			}
			{
				close_win = new Button(this, SWT.PUSH | SWT.CENTER);
				close_win.setText("\u53d6\u6d88");
				close_win.setBounds(386, 439, 60, 30);
				close_win.addSelectionListener(new SelectionAdapter() {   
					public void widgetSelected(SelectionEvent e) {   
						getShell().close();
					}   
				});  
			}
			{
				menu1 = new Menu(getShell(), SWT.BAR);
				getShell().setMenuBar(menu1);
				{
					helpMenuItem = new MenuItem(menu1, SWT.CASCADE);
					helpMenuItem.setText("\u5e2e\u52a9");
					{
						helpMenu = new Menu(helpMenuItem);
						{
							contentsMenuItem = new MenuItem(helpMenu, SWT.CASCADE);
							contentsMenuItem.setText("\u64cd\u4f5c\u8bf4\u660e");
							contentsMenuItem.addSelectionListener(new SelectionAdapter() {   
								public void widgetSelected(SelectionEvent e) {  
									HelpWin.getInstance();									
								}   
							});  
						}
						{
							aboutMenuItem = new MenuItem(helpMenu, SWT.CASCADE);
							aboutMenuItem.setText("\u5173\u4e8e");
							aboutMenuItem.addSelectionListener(new SelectionAdapter() {   
								public void widgetSelected(SelectionEvent e) {  
									int style = SWT.ABORT |SWT.YES;
									MessageBox messageBox = new MessageBox(getShell(), style);
									messageBox.setText("关于");
									messageBox.setMessage("数据中心代码平台代码生成器V1.0\n\n\t2013年05月02日\n\n作者：韦少甲(weishaojia@revenco.com)\t");
								    int rc = messageBox.open();
								}   
							});  
						}
						helpMenuItem.setMenu(helpMenu);
					}
				}
			}
			this.layout();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getInstance() {
		Display display = Display.getDefault();
		Shell shell = new Shell(display,SWT.CLOSE);
		DcGen inst = new DcGen(shell, SWT.NULL);
		Point size = inst.getSize();
		shell.setLayout(new FillLayout());
		shell.layout();
		shell.setText("数据中心代码平台代码生成器V1.0");
		shell.setImage(new Image(display, "title.gif"));
		if(size.x == 0 && size.y == 0) {
			inst.pack();
			shell.pack();
		} else {
			Rectangle shellBounds = shell.computeTrim(0, 0, size.x, size.y);
			shell.setSize(shellBounds.width, shellBounds.height);
		}
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}
	
	private void show_tablesMouseDown(MouseEvent evt) {
		System.out.println("show_tables.mouseDown, event="+evt);
		//TODO add your code for show_tables.mouseDown
	}

}
