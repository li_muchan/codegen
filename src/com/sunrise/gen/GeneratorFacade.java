package com.sunrise.gen;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;

import com.sunrise.gen.provider.db.DbTableFactory;
import com.sunrise.gen.provider.db.DbTableGeneratorModelProvider;
import com.sunrise.gen.provider.db.model.Table;
import com.sunrise.gen.provider.java.JavaClassGeneratorModelProvider;
import com.sunrise.gen.provider.java.model.JavaClass;
/**
 * 
 * @author badqiu
 *
 */
public class GeneratorFacade {
	
	
	public GeneratorFacade() {
	}
	
	public void printAllTableNames() throws Exception {
		List tables = DbTableFactory.getInstance().getAllTables();
		System.out.println("\n----All TableNames BEGIN----");
		for(int i = 0; i < tables.size(); i++ ) {
			String sqlName = ((Table)tables.get(i)).getSqlName();
			System.out.println("g.generateTable(\""+sqlName+"\");");
		}
		System.out.println("----All TableNames END----");
	}
	
	public void generateByAllTable() throws Exception {
		List<Table> tables = DbTableFactory.getInstance().getAllTables();
		for(int i = 0; i < tables.size(); i++ ) {
			generateByTable(tables.get(i).getSqlName());
		}
	}
	
	/**
	 * add by SK.Loda
	 * @param regex 匹配正则表达式
	 * @param types 类型过滤 ，可选值:"TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL TEMPORARY", "ALIAS", "SYNONYM". 
	 * @return
	 * @throws Exception
	 */
	public void generateByConfig(String regex,String [] types) throws Exception {
		List<Table> tables = DbTableFactory.getInstance().getTablesByConfig(types);
		
		PatternCompiler compiler = new Perl5Compiler();
		Pattern pattern = compiler.compile(regex,Perl5Compiler.CASE_INSENSITIVE_MASK);
		PatternMatcher matcher = new Perl5Matcher();
		
		for(int i = 0; i < tables.size(); i++ ) {
			String tableName = tables.get(i).getSqlName();
			
			if(matcher.matches(tableName, pattern))
				generateByTable(tableName);
		}
	}
	
	public void generateByMulTable(String tableNames)throws Exception{
		String[] tables=tableNames.split(",");
		for(int i=0;i<tables.length;i++){
			generateByTable(tables[i]);
		}
	}
	
	public void generateByTable(String tableName) throws Exception {
		Generator g = createGeneratorForDbTable();
		Table table = DbTableFactory.getInstance().getTable(tableName);
		g.generateByModelProvider(new DbTableGeneratorModelProvider(table));
	}
	
	public void generateByTable(String tableName,String className) throws Exception {
		Generator g = createGeneratorForDbTable();
		Table table = DbTableFactory.getInstance().getTable(tableName);
		table.setClassName(className);
		g.generateByModelProvider(new DbTableGeneratorModelProvider(table));
	}
	
	public void generateByClass(Class clazz) throws Exception {
		Generator g = createGeneratorForJavaClass();
		g.generateByModelProvider(new JavaClassGeneratorModelProvider(new JavaClass(clazz)));
	}
	
	public void clean() throws IOException {
		Generator g = createGeneratorForDbTable();
		g.clean();
	}
	
	public Generator createGeneratorForDbTable() {
		Generator g = new Generator();
		g.setEncoding(GeneratorProperties.getRequiredProperty("encoding"));
		g.setTemplateRootDir(new File(GeneratorProperties.getRequiredProperty("templateRoot")).getAbsoluteFile());
		g.setOutRootDir(GeneratorProperties.getRequiredProperty("outRoot"));
		return g;
	}
	
	private Generator createGeneratorForJavaClass() {
		Generator g = new Generator();
		g.setEncoding(GeneratorProperties.getRequiredProperty("encoding"));
		g.setTemplateRootDir(new File(GeneratorProperties.getRequiredProperty("template")+"/javaclass").getAbsoluteFile());
		g.setOutRootDir(GeneratorProperties.getRequiredProperty("outRoot"));
		return g;
	}
}
