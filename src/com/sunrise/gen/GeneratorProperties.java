package com.sunrise.gen;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sunrise.dc.MainVO;
import com.sunrise.gen.util.PropertiesHelper;


/**
 * 用于装载generator.properties文件
 * @author badqiu
 * @email badqiu(a)gmail.com
 */
public class GeneratorProperties {

	
	static PropertiesHelper props;
	static MainVO vo=null;
	private GeneratorProperties(){}
	public static void init(MainVO vo){GeneratorProperties.vo=vo;getHelper();}
	
	private static void loadProperties() {
		try {
			//System.out.println("Load [generator.properties] from classpath");
			props = new PropertiesHelper(loadAllPropertiesByClassLoader());
			
			String basepackage = getRequiredProperty("basepackage");
			String basepackage_dir = basepackage.replace('.', '/');
			props.setProperty("basepackage_dir", basepackage_dir);
			
			for(Iterator it = props.entrySet().iterator();it.hasNext();) {
				Map.Entry entry = (Map.Entry)it.next();
				System.out.println("[Property] "+entry.getKey()+"="+entry.getValue());
			}
			
			System.out.println();
			
		}catch(IOException e) {
			throw new RuntimeException("Load Properties error",e);
		}
	}
	
	public static Properties getProperties() {
		return getHelper().getProperties();
	}
	
	private static PropertiesHelper getHelper() {
		if(props == null)
			loadProperties();
		return props;
	}
	
	public static String getProperty(String key, String defaultValue) {
		return getHelper().getProperty(key, defaultValue);
	}
	
	public static String getProperty(String key) {
		return getHelper().getProperty(key);
	}
	
	public static String getRequiredProperty(String key) {
		return getHelper().getRequiredProperty(key);
	}
	
	public static int getRequiredInt(String key) {
		return getHelper().getRequiredInt(key);
	}
	
	public static boolean getRequiredBoolean(String key) {
		return getHelper().getRequiredBoolean(key);
	}
	
	public static String getNullIfBlank(String key) {
		return getHelper().getNullIfBlank(key);
	}
	
	public static void setProperty(String key,String value) {
		getHelper().setProperty(key, value);
	}
	
	public static void setProperties(Properties v) {
		props = new PropertiesHelper(v);
	}

	public static Properties loadAllPropertiesByClassLoader() throws IOException {
		Properties properties = new Properties();
		properties.put("basepackage",vo.getPack());
		properties.put("jdbc.url",vo.getUrl());
		properties.put("jdbc.driver",vo.getDrv());
		properties.put("outRoot",vo.getPath());
		properties.put("jdbc.username",vo.getUser());
		properties.put("jdbc.password",vo.getPass());
		properties.put("author",vo.getAuthor());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		properties.put("dateTime", sdf.format(new Date()));
		properties.put("namespace", "pages");
		properties.put("hibernate_id_generator", "native");
		properties.put("templateRoot", "template");
		properties.put("encoding", "UTF-8");
		if(vo.getDrv().toLowerCase().indexOf("oracle")!=-1){
			properties.put("jdbc.schema", vo.getUser().toUpperCase());
		}
		System.out.println(properties);
		return properties;
	}
}
