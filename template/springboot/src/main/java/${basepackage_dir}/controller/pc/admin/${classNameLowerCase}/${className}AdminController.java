<#include "/macro.include"/>
<#assign className = table.className>  
<#assign classNameLower = className?uncap_first> 
<#assign classNameCap = className?upper_case> 
<#assign classNameLowereCase = className?lower_case> 
package ${basepackage}.controller.pc.admin.${classNameLowereCase};

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import  ${basepackage}.common.MVCViewName;
import  ${basepackage}.model.${className}Criteria;
import  ${basepackage}.model.${className}Criteria.Criteria;
import  ${basepackage}.model.ao.${className}AO;
import ${basepackage}.service.I${className}Service;

import com.benfan.framework.common.model.ServiceResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiumi.util.PageUtil;
@Controller
@RequestMapping("/admin/${classNameLowereCase}")
public class ${className}AdminController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass()); 

	@Autowired
	I${className}Service ${classNameLower}Service;
	
	/**
	 * form表单提交 Date类型数据绑定
	 * <功能详细描述>
	 * @param binder
	 * @see [类、类#方法、类#成员]
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	@RequestMapping("/pagelist")
	@ResponseBody
	@RequiresPermissions("${classNameLowereCase}:pagelist")
	public PageUtil pageList(HttpServletRequest request,${className}AO ao){
		String page = request.getParameter("page");
		String pagesize = request.getParameter("pagesize");
		String sortName = request.getParameter("sortName");
		String sortOrder = request.getParameter("sortOrder");
		String search = request.getParameter("search");
		if(StringUtils.isNotBlank(sortName)&&StringUtils.isNotBlank(sortOrder)){
			String order = com.xiumi.util.StringUtils.humpToLine2(sortName) +" "+sortOrder;
			PageHelper.orderBy(order);
		}
		if(StringUtils.isBlank(page)){
			page="0";
		}
		if(StringUtils.isBlank(pagesize)){
			pagesize="10";
		}
		PageHelper.startPage(Integer.parseInt(page), Integer.parseInt(pagesize));
        ${className}Criteria uc = new ${className}Criteria();
        Criteria ucc =  uc.createCriteria();
        // 关键字模糊查询 
        if(StringUtils.isNotBlank(search)){
			
		}
        ServiceResult<List<${className}AO>> ${classNameLower}s = ${classNameLower}Service.selectByCriteria(uc);
        return new PageUtil(new PageInfo(${classNameLower}s.getData())); 
	}
	/**
	 * 跳转列表页
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value ="/list"  )
	@RequiresPermissions("${classNameLowereCase}:list")
	public String list(HttpServletRequest request, HttpServletResponse response,Model model) {
		return MVCViewName.XIUMI_PC_ADMIN_${classNameCap}_LIST.toString();
	}
	/**
	 * 跳转新增页
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value ="/add" )
	@RequiresPermissions("${classNameLowereCase}:add")
	public String add(HttpServletRequest request, HttpServletResponse response,Model model) {
		model.addAttribute("data", new ${className}AO());
		return MVCViewName.XIUMI_PC_ADMIN_${classNameCap}_ADD.toString();
	}
	/**
	 * 跳转编辑
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value ="/edit"  )
	@RequiresPermissions("${classNameLowereCase}:edit")
	public String edit(HttpServletRequest request, String id,Model model) {
		${className}AO ao = new ${className}AO();
		if(StringUtils.isNotBlank(id)){
			ao= ${classNameLower}Service.selectByPrimaryKey(id).getData();
		}
		model.addAttribute("data", ao);
		return MVCViewName.XIUMI_PC_ADMIN_${classNameCap}_EDIT.toString();
	}
	
	/**
	 * 保存或修改对象信息
	 * @param ao
	 * @return
	 */
	@RequestMapping(value ="/save"  )
	@RequiresPermissions("${classNameLowereCase}:save")
	@ResponseBody
	public Object save(HttpServletRequest request,${className}AO ao){
		ServiceResult<Boolean> ret = ${classNameLower}Service.saveOrUpdate(ao);
		return ret;
	}
	/**
	 * 根据ID 删除对象信息 物理删除
	 * @param id
	 * @return
	 */
	@RequestMapping(value ="/delete"  )
	@RequiresPermissions("${classNameLowereCase}:delete")
	@ResponseBody
	public Object delete(HttpServletRequest request, String ids ){
		if(StringUtils.isBlank(ids)){
			return new ServiceResult<Boolean>(false,"id不可为空");
		}
		String[] id = ids.split(",");
		List<String> idList = new ArrayList<String>();
		Collections.addAll(idList, id);
		${className}Criteria uc = new ${className}Criteria();
		uc.createCriteria().andIdIn(idList);
		return ${classNameLower}Service.deleteByCriteria(uc);
	}
	/**
	 * 根据ID 删除对象信息 逻辑删除
	 * @param id
	 * @return
	 
	@RequestMapping(value ="/deletelogic"  )
	@RequiresPermissions("${classNameLowereCase}:deletelogic")
	@ResponseBody
	public Object deletelogic(String id){
		if(StringUtils.isBlank(id)){
			return new ServiceResult<Boolean>(false,"id不可为空");
		}
		${className}AO ao = new ${className}AO();
		ao.setStatus("0");
		ao.setId(id);
		return ${classNameLower}Service.saveOrUpdate(ao);
	}
	*/
	
}
