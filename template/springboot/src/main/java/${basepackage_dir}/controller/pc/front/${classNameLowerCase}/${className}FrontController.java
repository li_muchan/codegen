<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
<#assign classNameCap = className?upper_case> 
<#assign classNameLowereCase = className?lower_case> 
package ${basepackage}.controller.pc.front.${classNameLowereCase};

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ${basepackage}.common.MVCViewName;
import ${basepackage}.model.${className}Criteria;
import ${basepackage}.model.${className}Criteria.Criteria;
import ${basepackage}.model.ao.${className}AO;
import ${basepackage}.service.I${className}Service;
import com.benfan.framework.common.model.ServiceResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping("/front/${classNameLower}")
public class ${className}FrontController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass()); 

	@Autowired
	I${className}Service ${classNameLower}Service;
	
	@RequestMapping("/pagelist")
	@ResponseBody
	@RequiresPermissions("${classNameLower}:pagelist")
	public PageInfo pageList(HttpServletRequest request,${className}AO ao, int page,int pageSize){
		if(page == 0 ){
			page=0;
		}
		if(pageSize==0){
			pageSize=10;
		}
        PageHelper.startPage(page, pageSize);  
        ${className}Criteria uc = new ${className}Criteria();
        Criteria ucc =  uc.createCriteria();
        ServiceResult<List<${className}AO>> ${classNameLower}s = ${classNameLower}Service.selectByCriteria(uc);
        return new PageInfo(${classNameLower}s.getData());  
	}
}
