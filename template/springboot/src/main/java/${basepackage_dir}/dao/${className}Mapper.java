<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.dao;

import java.util.List;

import ${basepackage}.model.${className};
import ${basepackage}.model.${className}Criteria;
import ${basepackage}.model.ao.${className}AO;
import com.benfan.framework.dao.BaseGeneratedMapper;

public interface ${className}Mapper extends BaseGeneratedMapper<${className}AO, ${className}Criteria>{
	<#list table.columns as column>
		<#if column.fk>
		public List<${className}AO> selectByFk${column.columnNameLower}(String ${column.columnNameLower});
		public int deleteByFk${column.columnNameLower}(String ${column.columnNameLower});
		</#if>
	</#list>
	<#if table.auto>
		public int insertBatch(List<${className}AO> list);
	</#if>
}
