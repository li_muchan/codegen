<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.dao.customized;

import java.util.List;

import ${basepackage}.model.${className};
import ${basepackage}.model.${className}Criteria;
import ${basepackage}.model.ao.${className}AO;
import com.benfan.framework.dao.BaseCustomizedMapper;
public interface ${className}CustMapper extends BaseCustomizedMapper<${className}AO, ${className}Criteria>{
}
