<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.model;

import java.io.Serializable;
import java.util.Date;

public class ${className} implements Serializable {
	<#list table.columns as column>
	<#if column.pk && column.javaType == 'java.lang.Long'>
	private String ${column.columnNameLower};
		<#else>
	private ${column.javaType} ${column.columnNameLower};
	</#if>
	
	</#list>
	<@generateConstructor className/>

	<@generateJavaColumns/>

	<#macro generateJavaColumns>
		<#list table.columns as column>

		<#if column.pk>
		public void set${column.columnNameFirstUpper}(String value) {
			this.${column.columnNameLower} = value;
		}	
		public String get${column.columnNameFirstUpper}() {
			return this.${column.columnNameLower};
		}
		</#if>	
		<#if !column.pk >
		public void set${column.columnNameFirstUpper}(${column.javaType} value) {
			<#if column.javaType=='java.lang.String'>
				this.${column.columnNameLower} = value == null ? null : value.trim();
				<#else>
				this.${column.columnNameLower} = value;
			</#if>
			
		}	
		public ${column.javaType} get${column.columnNameFirstUpper}() {
			return this.${column.columnNameLower};
		}
		</#if>
		</#list>
	</#macro> 

	<#list table.columns as column>
		<#assign crs = (column.remarks!'')?split(";")>
		<#if (crs?size>1)>
			public ${column.javaType} get${column.columnNameFirstUpper}Str() {
				<#assign crs = (column.remarks!'')?split(";")>
				 <#if (crs?size>1)>
				 	if(get${column.columnNameFirstUpper}()==null){
				 		return "";
				 	}
				 	switch (get${column.columnNameFirstUpper}()) {
				 	<#list crs as cr>
				 		<#if cr_index !=0>
				 		case "${cr?split("-")[0]}": return "${cr?split("-")[1]}";
				 		</#if>
				 	</#list>
					default:
						return get${column.columnNameFirstUpper}();
					}
		         	<#else>
		     		return get${column.columnNameFirstUpper}();
		     	</#if>
			}
		</#if>
	</#list>
	
}