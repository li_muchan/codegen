<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.service;

import java.util.List;
import ${basepackage}.model.ao.${className}AO;
import ${basepackage}.model.${className}Criteria;
import com.benfan.framework.service.IBaseAOService;


public interface I${className}Service extends IBaseAOService<${className}AO, ${className}Criteria> {


}

