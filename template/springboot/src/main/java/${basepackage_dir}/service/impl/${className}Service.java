<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${basepackage}.dao.${className}Mapper;
import ${basepackage}.model.${className};
import ${basepackage}.model.ao.${className}AO;
import ${basepackage}.model.${className}Criteria;
import ${basepackage}.service.I${className}Service;
import ${basepackage}.dao.customized.${className}CustMapper;
import com.benfan.framework.service.AbstractBaseAOService;
import com.benfan.framework.dao.BaseGeneratedMapper;



@Service
public class ${className}Service extends AbstractBaseAOService<${className}AO, ${className}Criteria>   implements I${className}Service {

    @Resource
    private ${className}Mapper ${classNameLower}GeneratedMapper;
    @Resource
    private ${className}CustMapper ${classNameLower}CustMapper;

    @Override
    protected BaseGeneratedMapper<${className}AO, ${className}Criteria> getGeneratedMapper() {
        return ${classNameLower}GeneratedMapper;
    }
}
