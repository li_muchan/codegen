<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
<#assign classNameLowerCase = table.classNameLowerCase>
${r" <#include"} "/xiumi/pc/admin/common/bootstrap_head.ftl">
                    <div class="ibox-content">
                        <form class="form-horizontal m-t" id="signupForm">
                        <input type="hidden" name="id" value="${r"${data.id!}"}"/>
					        			<#list table.columns as column>
					        	<#if !column.pk >
					        		<#assign rmsp = column.remarks?split(";")>
					        		<#if (rmsp?size >1)>
					        			<div class="form-group">
									        <label class="col-sm-3 control-label">${rmsp[0]}:</label>
									        <div class="col-sm-3">
									            <select class="form-control" name="${column.columnNameLower}">
									            <#list rmsp as sp>
									            	<#assign rmsp2 = sp?split("-")>
									            	<#if !(sp_index==0) && (rmsp2?size>0)>
									            		<option value='${rmsp2[0]}' ${r" <#if "} (data.${column.columnNameLower}!'')== '${rmsp2[0]}'>selected${r"</#if>"} >${rmsp2[1]!}</option>
									            	</#if>
									            	
									            </#list>
									            </select>
									        </div>
									    </div>
					        			<#else>
					        			<div class="form-group">
			                                <label class="col-sm-3 control-label">${column.remarks!}:</label>
			                                <div class="col-sm-3">
			                                    <input id="${column.columnNameLower}" name="${column.columnNameLower}" 
			                                    <#if column.sqlTypeName =="TIMESTAMP" || column.sqlTypeName =="DATETIME">
			                                    	${r"<#if "}data.${column.columnNameLower}??> value='${r"${data."}${column.columnNameLower}?datetime}'${r"</#if>"} 
			                                    <#else>
			                                    value='${r"${data."} ${column.columnNameLower}!}' 
			                                    </#if>
			                                    class="form-control" 
			                                     <#if column.sqlTypeName=='INT'>
			                                     type="number" maxlength="${column.size!0}"
			                                     <#else>
			                                      type="text"
			                                     </#if> >
			                                </div>
			                            </div>
					        		</#if>
					        	</#if>
					      	</#list>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <button class="btn btn-primary" type="button" id="button_submit">提交</button>
                                </div>
                            </div>
                        </form>
                    </div>
${r" <#include"} "/xiumi/pc/admin/common/bootstrap_bottom.ftl">
    <script src="${r" ${staticServePath}"}/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="${r" ${staticServePath}"}/js/plugins/validate/messages_zh.min.js"></script>
    <script src="${r" ${staticServePath}"}/js/admin/${classNameLowerCase}/form-validate-${classNameLowerCase}.min.js"></script>

