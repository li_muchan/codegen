<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
<#assign classNameLowerCase = table.classNameLowerCase>
${r" <#include"} "/xiumi/pc/admin/common/bootstrap_head.ftl">


<div class="col-sm-12">
    <!-- Example Events -->
    <div class="example-wrap">
        
        <div class="example">
            
            <div class="btn-group hidden-xs" id="exampleTableEventsToolbar" role="group">
            	${r" <@shiro.hasPermission"} name="${classNameLowerCase}:add">
                <button type="button" class="btn btn-outline btn-default" onclick="layerAdd('${classNameLowerCase}','${classNameLowerCase}')">
                    <i class="glyphicon glyphicon-plus" aria-hidden="true" >新增</i>
                </button>
                ${r" </@shiro.hasPermission"}>
                <!-- <button type="button" class="btn btn-outline btn-default">
                    <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                </button> -->
                ${r" <@shiro.hasPermission"} name="${classNameLowerCase}:edit">
                <button type="button" class="btn btn-outline btn-default" onclick="layerEdit('${classNameLowerCase}','${classNameLowerCase}')">
				 <i class="glyphicon glyphicon-pencil" aria-hidden="true">修改</i>
				 </button>
				 ${r" </@shiro.hasPermission"}>
				 ${r" <@shiro.hasPermission"} name="${classNameLowerCase}:delete">
                <button type="button" class="btn btn-outline btn-default" onclick="layerDel('${classNameLowerCase}')">
                    <i class="glyphicon glyphicon-trash" aria-hidden="true">删除</i>
                </button>
                ${r" </@shiro.hasPermission"}>
            </div>
            <table id="exampleTableEvents"  data-mobile-responsive="true">
            </table>
        </div>
    </div>
</div>
${r" <#include"} "/xiumi/pc/admin/common/bootstrap_bottom.ftl">
<script src="${r" ${staticServePath}"}/js/admin/${classNameLowerCase}/bootstrap-table-${classNameLowerCase}.min.js"></script>
