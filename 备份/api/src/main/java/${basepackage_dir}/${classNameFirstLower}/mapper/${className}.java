<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
/**
 * ${basepackage}.mapper.${className}.java
 */
package ${basepackage}.${classNameLower}.mapper;

import com.foodbank.commons.utils.BaseVO;

/**
 * @file  ${className}.java
 * @author ${author}
 * @version 0.1
 * @${className}实体类
 * Copyright(C), 2013-2014
 *		 Guangzhou Sunrise Technology Co., Ltd.
 * History
 *   	1. Date: ${dateTime}
 *      	Author: ${author}
 *      	Modification: this file was created
 *   	2. ...
 */
public class ${className} extends BaseVO implements java.io.Serializable{

	<#list table.columns as column>
	private ${column.javaType} ${column.columnNameLower};
	</#list>
<@generateConstructor className/>

<@generateJavaColumns/>

<#macro generateJavaColumns>
	<#list table.columns as column>

	<#if column.pk>
	public void set${column.columnNameFirstUpper}(${column.javaType} value) {
		this.${column.columnNameLower} = value;
	}	
	public ${column.javaType} get${column.columnNameFirstUpper}() {
		return this.${column.columnNameLower};
	}
	</#if>	
	<#if !column.pk && column.columnNameLower != "creater" && column.columnNameLower != "modifier" && column.columnNameLower != "createTime" && column.columnNameLower != "modifyTime">
	public void set${column.columnNameFirstUpper}(${column.javaType} value) {
		this.${column.columnNameLower} = value;
	}	
	public ${column.javaType} get${column.columnNameFirstUpper}() {
		return this.${column.columnNameLower};
	}
	</#if>
	</#list>
</#macro> 

    public String toString() {
		  StringBuffer buffer = new StringBuffer();
		  buffer.append(getClass().getName()).append("@").append(Integer.toHexString(hashCode())).append(" [");	
		  <#list table.columns as column>
			   buffer.append("${column.columnName}").append("='").append(get${column.columnName}()).append("' ");	
		  </#list>
		  buffer.append("]");
		  return buffer.toString();
	}		

	public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ${className}) ) return false;
		 ${className}  castOther = ( ${className}) other;  
		 return 
			 <#list table.columns as column>
			   <#if column.pk >
					( (this.get${column.columnName}()==castOther.get${column.columnName}()) ||( this.get${column.columnName}()!=null && castOther.get${column.columnName}()!=null && this.get${column.columnName}().equals(castOther.get${column.columnName}()) ) )
			   </#if>
			   <#if !column.pk >
					&&( (this.get${column.columnName}()==castOther.get${column.columnName}()) ||( this.get${column.columnName}()!=null && castOther.get${column.columnName}()!=null && this.get${column.columnName}().equals(castOther.get${column.columnName}()) ) )
			    </#if>
			 </#list>
			;
    }

	public int hashCode() {
         int result = 17;
			 <#list table.columns as column>
				  result = 37 * result + (get${column.columnName}() == null ? 0 : this.get${column.columnName}().hashCode() );
			</#list>
         return result;
	} 
	
}
