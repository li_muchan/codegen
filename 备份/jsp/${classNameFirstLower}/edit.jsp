<#include "/macro.include"/>
<#assign className = table.className>   
<#assign sqlName = table.sqlName>   
<#assign classNameLower = className?uncap_first> 
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  	<head>
   		<%@ include file="/resources/public/head.jspf" %>
</head>
	<body>
	   	<form id="ff" method="post" enctype="multipart/form-data">
	   		<#list table.columns as column>
	   			<#if !column.pk >
				<div>   
			        <label for="${column.columnNameLower}">${column.remarks}:</label>   
			        <input  type="text" name="${column.columnNameLower}"/>   
			    </div>
			    <#else>
			    	<input type="hidden" name="${column.columnNameLower}">
			    </#if>
			</#list>
		    <div>
		    	<a id="edit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-edit'">修改</a> 
		    	<a id="reset" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-reload'">重置</a>   
		    </div>
		</form>
	</body>
<script>
	$(function(){
		var ue = UE.getEditor('contentue');
		var dg = parent.$("iframe[title='${sqlName}']").get(0).contentWindow.$("#dg");
		var row = dg.datagrid("getSelected");
		$("#ff").form("load",{
			<#list table.columns as column>
				<#if !column_has_next>
					${column.columnNameLower}:row.${column.columnNameLower}
				<#else>
					${column.columnNameLower}:row.${column.columnNameLower},
				</#if>
			</#list>
		});
		ue.ready(function() {
			ue.setContent(row.content);
		});
		<#list table.columns as column>
		<#if !column.pk>
			<#if column.javaType=="java.lang.String">
				$("input[name='${column.columnNameLower}']").validatebox({
					required:true,
					missingMessage:"请填写${column.remarks}"
				});
			<#elseif column.javaType=="java.lang.Integer">
				$("input[name='${column.columnNameLower}']").numberbox({
					required: true,  
		   		 	missingMessage:"请输入${column.remarks}",
				    min:0
				});
			<#elseif column.javaType=="java.lang.Double">
				$("input[name='${column.columnNameLower}']").numberbox({
					required: true,  
		   		 	missingMessage:"请输入${column.remarks}",
				    min:0,    
				    precision:2,
				 	prefix:"$"
				});
				</#if>
		</#if>
		</#list>
		$("#ff").form("disableValidation");
		$("#edit").click(function(){
			$("#ff").form("enableValidation");
			if($("#ff").form("validate")){
				$("#ff").form("submit",{
					url: "admin/${classNameLower}/update.do",
					success: function(){
						parent.$("#win").window("close");
						parent.$("iframe[title='${sqlName}']").get(0).contentWindow.$("#dg").datagrid("reload");
					}
				});
			}
		});
		$("#reset").click(function(){
			$("#ff").form("reset");
		});
	});
</script>
</html>
