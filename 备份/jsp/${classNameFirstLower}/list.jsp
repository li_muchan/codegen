<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  	<head>
   		<%@ include file="/resources/public/head.jspf" %>
</head>
	<body>
	   	<table id="dg"></table>
	</body>
	<script>
		function deleteOpe(ids){
			$.messager.confirm('确认','您确认想要删除记录吗？',function(e){
				if(e){
					$.ajax({
			    		url:"admin/${classNameLower}/delete.do",
			    		type:"POST",
			    		data:{
			    			ids:ids
			    		},
			    		success:function(result){
			    			if(result>0){
			    				$.messager.alert("删除提示","删除成功!","info",function(){
			    					$("#dg").datagrid("reload");
			    				});
			    			}else{
			    				$.messager.alert("删除提示","删除失败!","info");
			    			};
			    		}
			    	});
				}
			});
		}
		function edit(index){
			parent.$("#win").window({
				title:"修改",
				width:900,
				height:650,
				content:'<iframe src="${r'${basePath}'}/send-admin-${classNameLower}-edit.do" frameborder="0" width="100%" height="100%"></iframe>',
				onClose:function(){
					$("#dg").datagrid("reload");
				},
				onBeforeOpen:function(){
					$("#dg").datagrid("uncheckAll");
					setTimeout(function(){$("#dg").datagrid("checkRow",index);},1);
				}
			});
		}
		$(function(){
			$('#dg').datagrid({    
			    url:'${r'${basePath}'}/send-admin-${classNameLower}-edit.do/list.do',
			    pagination:true,
			    fit:true,
			    columns:[[
			    {field:'',checkbox:true,width:'7%'}
			    <#list table.columns as column>
				,{field:'${column.columnNameLower}',title:'${column.remarks}',width:'7%',align:'center'}
				</#list>
			        ,{field:'opera',title:'操作',width:'7%',align:'center',formatter:function(value,row,index){
			        	return "<shiro:hasPermission name='${classNameLower}:update'><span style='color:green;' onclick='edit("+index+")'>修改</span></shiro:hasPermission><shiro:hasPermission name='${classNameLower}:delete'><span onclick='deleteOpe(\""+row.id+"\")' style='color:red;margin-left:5px;'>删除</span></shiro:hasPermission>";
			        }}
			    ]],
			    toolbar: [
			    <shiro:hasPermission name="${classNameLower}:add">
		             {
						iconCls: 'icon-add',
						text:'新增',
						handler: function(){
							parent.$('#win').window({
								title:'新增赛事活动',
								width:900,
								height:650,
							    content:'<iframe src="${r'${basePath}'}/send-admin-${classNameLower}-add.do" frameborder="0" width="100%" height="100%"></iframe>'
							});
						}
					},'-',
				</shiro:hasPermission>
				<shiro:hasPermission name="${classNameLower}:delete">
					{
						iconCls: 'icon-remove',
						text:'删除',
						handler: function(){
							var rows = $("#dg").datagrid("getSelections");
							if(rows.length>0){
								$.messager.confirm('删除确认','您确认想要删除记录吗？',function(r){
								    if (r){
								    	var ids = [];
								    	$.each(rows,function(index,value){
								    		ids.push(value.id);
								    	});
								    	deleteOpe(ids.toString());
								    };
								}); 
							}else{
								$.messager.alert('删除提示','至少选择一条记录删除！','info');
							}
						}
					},'-',
				</shiro:hasPermission>
					{
						text:'<input id="startTime" type="text"/>'
					},
					{
						text:'至'
					},
					{
						text:'<input id="endTime" type="text" placeholder="请输入结束时间"/>'
					},'-',
					{
						text:'<input id="searchName" type="text" placeholder="请输入活动名称" style="padding-left:10px;"/>'
					},'-',
					{
						iconCls:'icon-search',
						handler:function(){
							$('#dg').datagrid('load',{
								name: $.trim($("#searchName").val()),
								startTime:$('#startTime').datetimebox("getValue"),
								endTime:$('#endTime').datetimebox("getValue")
							});
						}
					},
					{
						iconCls:'icon-clear',
						handler:function(){
							$("#searchName").val('');
							$("#startTime").datetimebox('clear');
							$("#endTime").datetimebox('clear');
						}
					}
				]
			});
			$('#startTime').datetimebox({    
			    width:160,
			    editable:false,
			    prompt:'请输入起始时间'
			}); 
			$('#endTime').datetimebox({
			    width:160,
			    editable:false,
			    prompt:'请输入结束时间'
			}); 
		});
	</script>
</html>
