<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameFirstLower = className?uncap_first> 
/**
 * ${basepackage}.bo.${className}Bo.java
 */
package ${basepackage}.${classNameFirstLower};

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ${basepackage}.${classNameFirstLower}.mapper.${className};
/**
 * 
 */
public class ${className}Provider 
{
	static {
		PropertyConfigurator.configure("/foodbank/conf/${classNameFirstLower}-log4j.properties");
	}
	private static final Logger logger = Logger.getLogger(${className}.class);
	
	public static void main(String[] args) throws Exception
	{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {
				"classpath*:${classNameFirstLower}-datasource.xml",
				"classpath*:${classNameFirstLower}-provider.xml",
				"classpath*:${classNameFirstLower}-service.xml",
				"classpath*:${classNameFirstLower}-dao.xml"
				}
		);
		context.start();
		logger.info("${classNameFirstLower} provider is started ...");
//		System.in.read();
		while(true) {
			Thread.sleep(60000);
			logger.info("...");
		}
	}
}
