<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
/**
 * ${basepackage}.bo.${className}Bo.java
 */
package ${basepackage}.${classNameLower}.bo;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.foodbank.commons.utils.Page;
import com.foodbank.food.commons.define.WcsCommonStatus;

import ${basepackage}.${classNameLower}.dao.${className}Dao;
import ${basepackage}.${classNameLower}.mapper.${className};
import ${basepackage}.${classNameLower}.service.${className}Service;

import org.apache.commons.lang3.StringUtils;

/**
 * @file  ${className}Bo.java
 * @author ${author}
 * @version 0.1
 * @${className}业务操作接口实现类
 * Copyright(C), 2013-2014
 *		 Guangzhou Sunrise Technology Co., Ltd.
 * History
 *   	1. Date: ${dateTime}
 *      	Author: ${author}
 *      	Modification: this file was created
 *   	2. ...
 */
public class ${className}Bo implements ${className}Service {
	
	private ${className}Dao ${classNameLower}Dao;
	

    /**
	 * @param ${classNameLower}Dao the ${classNameLower}Dao to set
	 */
	public void set${className}Dao(${className}Dao ${classNameLower}Dao) {
		this.${classNameLower}Dao = ${classNameLower}Dao;
	}


	/**
	 * 查找所有数据库记录
     * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
     */
	@SuppressWarnings({ "rawtypes" })
    public List getAll() throws Exception {
		return ${classNameLower}Dao.selectAll();
	}
	
	/**
     * 查找符合条件的所有数据库记录
     * @param vo 与数据库中记录对应的值对象
     * @return type : List 返回查询操作所有符合条件的记录的VO对象集合，操作失败返回null
     * @throws Exception
     */
	@SuppressWarnings({ "rawtypes" })
    public List get(${className} vo) throws Exception{
		if(vo == null) return null;
        return ${classNameLower}Dao.selectByVo(vo);
	}
	
    /**
     * 分页查找
     * @param params 与数据库中记录对应的值对象
     * @return type : Page 返回满足条件的记录集，操作失败返回null
     * @throws Exception
     */
	@SuppressWarnings({ "rawtypes" })
    public Page getPageByParamMap(Map params) throws Exception{
		if (params == null) return null;
		return ${classNameLower}Dao.queryForPage(params);
	}
	
    /**
     * 查找符合条件的数据库记录
     * @param pkid 与数据库中主键对应的值
     * @return type : 返回查询操作符合条件的记录的VO对象，操作失败返回null
     * @throws Exception
     */
	<#list table.columns as c>
	<#if c.pk>
	<#if c.sqlTypeName=='LONG'|| c.sqlTypeName=='INT' || c.sqlTypeName=='INTEGER' || c.sqlTypeName=='NUMBER'>
	public ${className} get(Long pkId) throws Exception{
		if(pkId==null) return null;
	<#else>
	public ${className} get(String pkId) throws Exception{
		if(StringUtils.isEmpty(pkId)) return null;
	</#if>
	</#if>
	</#list>
     	return ${classNameLower}Dao.selectByPk(pkId);
	}
	
	/**
     * 向数据库中插入一条记录
     * @param vo 与数据库中记录对应的值对象
     * @return type : RpDir 返回插入操作是否成功
     * @throws Exception
     */
    public ${className} save(${className} vo) throws Exception{
    	if(vo == null) return null;		
		<#list table.columns as column>
			<#if column.pk> 				
		if(vo.get${column.columnNameFirstUpper}()!=null) {
			return ${classNameLower}Dao.updateByVo(vo);
		} else {
			return ${classNameLower}Dao.insertByVo(vo);
		}
			</#if>
		</#list>
    }
    
    /**
     * 删除符合条件的数据库记录
     * @param pkid 与数据库中主键对应的值
     * @return type : boolean 返回删除操做是否成功，操作失败返回false
     */
	<#list table.columns as c>
	<#if c.pk>
	<#if c.sqlTypeName=='LONG'|| c.sqlTypeName=='INT' || c.sqlTypeName=='INTEGER' || c.sqlTypeName=='NUMBER'>
    public boolean delete(Long pkId) throws Exception{
    	if(pkId == null) return false;
	<#else>
    public boolean delete(String pkId) throws Exception{
    	if(StringUtils.isEmpty(pkId)) return false;
	</#if>
	</#if>
	</#list>
    	return ${classNameLower}Dao.deleteByPk(pkId);
    }
    
    /**
	 * 逻辑删除数据库中的对象
	 * @param ids: Id列表, modifier: 修改者
	 * @return type : boolean 返回操作是否成功
	 * @throws Exception
	 */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean deleteLogical(List ids, String modifier) throws Exception{
    	Map params = new HashMap();
    	params.put("ids", ids);
    	params.put("status", WcsCommonStatus.WCS_INVALID);
    	params.put("modifier", modifier);
    	params.put("modifyTime", Calendar.getInstance().getTime());
    	return ${classNameLower}Dao.updateStatus(params);
	}
	
    /**
	 * 恢复已逻辑删除的数据库中的对象
	 * @param ids: Id列表, modifier: 修改者
	 * @return type : boolean 返回操作是否成功
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean recover(List ids, String modifier) throws Exception{
    	Map params = new HashMap();
    	params.put("ids", ids);
    	params.put("status", WcsCommonStatus.WCS_VALID);
    	params.put("modifier", modifier);
    	params.put("modifyTime", Calendar.getInstance().getTime());
    	return ${classNameLower}Dao.updateStatus(params);
	}
}
