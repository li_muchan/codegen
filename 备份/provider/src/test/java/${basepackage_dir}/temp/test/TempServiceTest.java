<#include "/macro.include"/>
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.temp.test;


import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ${basepackage}.${classNameLower}.mapper.${className};
import ${basepackage}.${classNameLower}.service.${className}Service;

/**
 * Unit test for simple App.
 */
public class TempServiceTest extends TestCase
{
	private ClassPathXmlApplicationContext context;
	private ${className}Service service;
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TempServiceTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( TempServiceTest.class );
    }
    
    public void setUp() {
    	context = new ClassPathXmlApplicationContext(
    			new String[] {
    				"classpath*:${classNameLower}-datasource.xml",
    				"classpath*:${classNameLower}-service.xml",
    				"classpath*:${classNameLower}-dao.xml"
    				});
//    	context = new ClassPathXmlApplicationContext(new String[]{"activity-consumer.xml"});
		context.start();
		
		service = (${className}Service) context.getBean("${classNameLower}Bo");
    }
    
    public void tearDown() {
    	
    }
	

	public void testGenerateCoupons() throws Exception {
		
//		Fuser user= new Fuser();
//		user.setId(4);
//		Fuser user1 = service.save(user);
//		System.out.println(user1.getMobile());
		
//		Fuser user2=service.get(Long.parseLong("4"));
//		System.out.println(user2.getMobile());
//		Map map = new HashMap<>();
//		map.put("currPage", 2);
//		map.put("pageSize", 2);
//		Page page =  service.getPageByParamMap(map);
////		System.out.println(page.getCurrentPage());
////		System.out.println(page.getCurrPageSize());
//		List<Fuser> list = page.getList();
//		for (Fuser user2 :list) {
//			System.out.println(user2.getId());
//		}
    }
}
